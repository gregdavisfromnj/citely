package org.blueclawsoft.citely.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Gregory on 6/21/2015.
 */
@Controller
public class TagsController {

    @GetMapping(value = "/tags")
    public String tags() {
        return "tags";
    }

}
