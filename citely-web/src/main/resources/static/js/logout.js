// Cancelling logout will navigate backwards in the history stack
cancel_button = document.getElementById('logoutForm').elements["logoutCancelBtn"]
if (cancel_button !== null) {
    cancel_button.onclick = function () {
        window.history.back();
    }
}
