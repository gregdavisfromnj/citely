Citely
======

Citely the Citation Manager


A GPL web-app citation manager. Stores citations for scholarly research on the network, allowing users
to tag citations in a folksonomy. Organizes and exports citations to common bibliographic formats.
Allows the data and access to be managed by you!
