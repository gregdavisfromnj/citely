// Show navbar item for current page as active.
let navLinks = document.querySelectorAll('#navLinks li');
if (navLinks !== null) {
  for (let listItem of navLinks) {
    let anchors = listItem.getElementsByTagName('a');
    if (anchors !== null) {
      for (let a of anchors) {
        let uri = a.getAttribute('href');
        if (uri !== null && uri === location.pathname) {
          listItem.classList.add('active');
        }
      }
    }
  }
}

// Set the logout button click action to add "logout" to the
// location fragment, for navigation purposes.
let logout_button = document.getElementById('logoutBtn')
if (logout_button !== null) {
  logout_button.onclick = function () {
    window.location.href = 'logout';
  }
}
